import time

from sqlalchemy import Integer, Column, String, BigInteger
from base import Base


class MoneroHeader(Base):
    __tablename__ = 'block_headers'
    block_size = Column('block_size', Integer)
    difficulty = Column('difficulty', BigInteger)
    hash = Column('hash', String(64))
    height = Column('height', Integer, primary_key=True)
    major_version = Column('major_version', String(10))
    minor_version = Column('minor_version', String(10))
    nonce = Column('nonce', BigInteger)
    prev_hash = Column('prev_hash', String(64))
    reward = Column('reward', BigInteger)
    timestamp = Column('timestamp', BigInteger)
    server_timestamp = Column('server_timestamp', BigInteger)

    def __init__(self, raw_json):
        self.block_size = raw_json["block_size"]
        self.difficulty = raw_json["difficulty"]
        self.hash = raw_json["hash"]
        self.height = raw_json["height"]
        self.major_version = raw_json["major_version"]
        self.minor_version = raw_json["minor_version"]
        self.nonce = raw_json["nonce"]
        self.prev_hash = raw_json["prev_hash"]
        self.reward = raw_json["reward"]
        self.timestamp = raw_json["timestamp"]
        # get unix time for comparison with actual block time
        self.server_timestamp = time.time()

    def __str__(self):
        return "BlocK: {} Hash: {} Diff: {}".format(self.height, self.hash, self.difficulty)
