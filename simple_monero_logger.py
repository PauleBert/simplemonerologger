import random
import time

from base import Base, psql_engine, session
from monero_handler import MoneroHandler
from monero_wrapper import MoneroHeader

if __name__ == '__main__':
    print("Starting simple <Monero> Logger")
    Base.metadata.create_all(psql_engine)
    instance = session.query(MoneroHeader).order_by(MoneroHeader.height.desc()).first()
    if instance:
        current_top = instance.height
    else:
        current_top = 0
    # maximum 10k requested block headers per monero node request
    step_size = 1000
    # sleep timer for the next request
    sleep_timer = 3
    monero_handler = MoneroHandler("node.xmr.to", 18081, True)
    while True:
        start = time.time()
        if current_top > monero_handler.get_network_height() - step_size:
            current_step = monero_handler.get_network_height() - current_top
        else:
            current_step = step_size
        headers = monero_handler.get_headers_range(current_top, current_top + current_step)
        current_top = current_top + len(headers)
        end = time.time()
        if len(headers) > 0:
            for item in headers:
                instance = session.query(MoneroHeader).filter_by(height=item.height).first()
                if instance:
                    pass
                else:
                    session.add(item)
            session.commit()
            print(headers[0])
        end = time.time()
        print("Fetched: {} in this run! Took about {}seconds!".format(len(headers), (end - start)))
        time.sleep(random.randint(sleep_timer, sleep_timer*10))
