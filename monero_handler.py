import json
from json import JSONDecodeError

import requests
from requests.auth import HTTPDigestAuth

from monero_wrapper import MoneroHeader

ERROR_MSG = -1
URL_BASE = "http://{}:{}/{}"
JSON_STRING = "json_rpc"


class MoneroHandler:

    def __init__(self, daemon_ip, daemon_port, mainnet=True):
        self.daemon_ip = daemon_ip
        self.daemon_port = daemon_port
        self.mainnet = mainnet
        self.url = URL_BASE.format(daemon_ip, daemon_port, JSON_STRING)

    def make_request(self, payload):
        try:
            headers = {'content-type': 'application/json'}
            data = json.dumps(payload)
            response = requests.post(self.url, data=data, headers=headers).json()
            return response
        except (IOError, JSONDecodeError) as e:
            print(str(self.url))
            print(str(payload))
            print(str(e))
            print("Fehler bei Verbindung mit Server")
            return ERROR_MSG

    def get_network_height(self):
        payload = {
            "jsonrpc": "2.0",
            "id": 0,
            "method": "get_block_count",
        }
        result = self.make_request(payload)
        if result == ERROR_MSG:
            return ERROR_MSG
        # -1 because arrays begin with 0
        return result["result"]["count"] - 1

    def get_headers_range(self, start, end):
        block_headers = []
        if start > end:
            return block_headers
        if start < 0 and end < 0:
            return block_headers
        if end > self.get_network_height():
            return block_headers
        payload = {
            "jsonrpc": "2.0",
            "id": 0,
            "method": "get_block_headers_range",
            "params": {
                "start_height": start,
                "end_height": end
            }
        }
        result = self.make_request(payload)
        if result == ERROR_MSG:
            return block_headers

        # now parse block headers into objects
        for header in result["result"]["headers"]:
            block_headers.append(MoneroHeader(header))
        return block_headers
